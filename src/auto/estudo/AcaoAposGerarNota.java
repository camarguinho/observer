package auto.estudo;

public interface AcaoAposGerarNota {

	public void executa(NotaFiscal nf);
	
}
