package auto.estudo;

public class TesteAcoes {

	public static void main(String[] args) {
		NotaFiscalBuilder builder = new NotaFiscalBuilder();
		builder.adicionaAcao(new EnviadorDeEmail());
		builder.adicionaAcao(new NotaFiscalDao());
		builder.adicionaAcao(new EnviadorDeSMS());
		builder.adicionaAcao(new Impressora());

		NotaFiscal nf = builder.paraEmpresa("CAP").comCnpj("123456").comObservacao("Nada a declarar").comItem(new ItemNota("Item 1", 10)).constroi();

		System.out.println(nf.getValorBruto());
	}

}
